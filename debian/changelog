ruby-webpacker (5.4.3-2+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 16:50:50 +0530

ruby-webpacker (5.4.3-2) unstable; urgency=medium

  * Team upload.
  * add patch to read yaml conf using unsafe_load (Closes: #1019677)

 -- Mohammed Bilal <mdbilal@disroot.org>  Sat, 03 Dec 2022 12:27:42 +0530

ruby-webpacker (5.4.3-1) experimental; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Rajesh Simandalahi ]
  * New upstream version 5.4.3
  * update and refresh patches
  * Bump debhelper from old 12 to 13
  * Bump Standards-Version to 4.6.1 (no changes needed)

  [ HIGUCHI Daisuke (VDR dai) ]
  * eliminate lintian warning: ruby-interpreter-is-deprecated
  * eliminate lintian warning: update-debian-copyright

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Wed, 10 Aug 2022 12:08:20 +0900

ruby-webpacker (5.4.0-1) experimental; urgency=medium

  * Team upload

  [ Cédric Boutillier ]
  * Add .gitattributes to keep unwanted files out of the source package

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove 1 unused lintian overrides.

  [ Sruthi Chandran ]
  * New upstream version 5.4.0

 -- Sruthi Chandran <srud@debian.org>  Sun, 01 Aug 2021 23:37:08 +0530

ruby-webpacker (4.2.2-7) unstable; urgency=medium

  * Team Upload
  * Add yarnpkg to Recommends
  * Disable tests that require yarnpkg (its testing migration is blocked,
    we can add it back when it enters testing)

 -- Pirate Praveen <praveen@debian.org>  Thu, 03 Sep 2020 19:34:26 +0530

ruby-webpacker (4.2.2-6) unstable; urgency=medium

  * Team Upload
  * Drop dependency on yarnpkg

 -- Pirate Praveen <praveen@debian.org>  Fri, 28 Aug 2020 14:46:46 +0530

ruby-webpacker (4.2.2-5) unstable; urgency=medium

  * Team Upload
  * Source only upload for testing migration

 -- Pirate Praveen <praveen@debian.org>  Fri, 07 Aug 2020 23:16:16 +0530

ruby-webpacker (4.2.2-4) unstable; urgency=medium

  * Team Upload
  * Don't exclude lib/install/bin

 -- Pirate Praveen <praveen@debian.org>  Tue, 04 Aug 2020 00:42:18 +0530

ruby-webpacker (4.2.2-3) unstable; urgency=medium

  * Team Upload
  * Update watch file to automaticaly repack
  * Remove generated files via debian/clean
  * Include .browserslistrc in file list
  * Add webpack as dependency

 -- Pirate Praveen <praveen@debian.org>  Mon, 03 Aug 2020 18:34:35 +0000

ruby-webpacker (4.2.2-2) unstable; urgency=medium

  * Team Upload
  * Skip failing check for nodejs version
  * Skip failing check for yarnpkg version

 -- Pirate Praveen <praveen@debian.org>  Mon, 03 Aug 2020 23:20:53 +0530

ruby-webpacker (4.2.2-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 4.2.2
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Mon, 03 Aug 2020 22:29:57 +0530

ruby-webpacker (4.0.7-1) unstable; urgency=medium

  * Initial release (Closes: #930918)

 -- Jongmin Kim <jmkim@pukyong.ac.kr>  Mon, 03 Aug 2020 16:48:09 +0530
